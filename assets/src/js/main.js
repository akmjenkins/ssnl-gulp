var ns = 'JAC-SSNL';
window[ns] = {};

// Bower Components
// @codekit-prepend "../../bower_components/pikaday/pikaday.js"

// Utilities used pretty much everywhere 
// @codekit-append "scripts/debounce.js"
// @codekit-append "scripts/tests.js"
// @codekit-append "scripts/image.loader.js"

// @codekit-append "scripts/anchors.external.popup.js"
// @codekit-append "scripts/custom.select.js"
// @codekit-append "scripts/lazy.images.js"
// @codekit-append "scripts/nav.js"
// @codekit-append "scripts/swiper.js"

// SSNL Specific Scripts
// @codekit-append "scripts/events.calendar.js"
// @codekit-append "scripts/team.logo.selector.js"

// @codekit-append "global.js"