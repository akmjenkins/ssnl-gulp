;(function(context) {

	$(document)
		.on('change','.selector.with-team-logo select',function() {
			var
				$el = $(this),
				logo = $el.find('option:selected').data('logo');

			$el.siblings('.team-logo').css('backgroundImage', logo ? 'url('+logo+')' : 'none');

		})
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));