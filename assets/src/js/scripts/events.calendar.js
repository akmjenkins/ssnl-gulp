;(function(context) {

	var EventsCalendar = function($el) {

		var self;

		this.$el = $el;
		this.cachedEvents = {};
		this.currentDate = null;
		this.src = this.$el.data('src');

		self = this;
		
		this.calendar = new Pikaday({
			onDraw: function() { 
				self.updateCalendar();
			},
			defaultDate: new Date()
		});
		$el.append(this.calendar.el)

	};

	EventsCalendar.prototype.getCalendarButtons = function() {
		return this.$el.find('td').filter(function() { return !$(this).hasClass('is-empty'); }).find('.pika-button');
	};

	EventsCalendar.prototype.updateCalendar = function() {
		var $buttons,
			data;

		$buttons = this.getCalendarButtons();
		data = $buttons.eq(0).data();
		this.currentDate = new Date(data.pikaYear,data.pikaMonth,1,0,0,0);


		//request the events from the server
		$.when(this.getEvents())
			.then(function(events) {
				
				//update all applicable buttons with a class of 'has-event'
				$buttons.each(function() {
					var $el,
						d;

					$el = $(this);
					d = $el.data();

					events.some(function(e) {
						if((d.pikaMonth+1) == e.month && d.pikaYear == e.year && d.pikaDay == e.day) {
							$el.addClass('has-event');
							return true;
						}
					});

				})

			});

	};

	EventsCalendar.prototype.getEvents = function() {
		var self,
			cacheKey;

		self = this;
		cacheKey = this.currentDate.getTime();
		if(this.cachedEvents.hasOwnProperty(cacheKey)) {
			return this.cachedEvents[cacheKey];
		}


		//request them from the server
		return $.ajax({
			url: templateJS.templateURL + '/' + this.src,
			dataType: 'json',
			data: {
				month: this.currentDate.getMonth()+1,
				year: this.currentDate.getFullYear()
			}
		})
			.then(function(r,status,jqXHR) {
				if(status === 'success' && Array.isArray(r) && r.length) {
					self.cachedEvents[cacheKey] = r;
					return self.cachedEvents[cacheKey];
				}

				//failed
				return $.Deferred().reject().promise();
			});
	};


	$('.events-calendar').each(function() {
		var $el = $(this);
		$el.data('eventsCalendar',(new EventsCalendar($el)));
	})
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));