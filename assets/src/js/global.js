;(function(context) {

	var tests;
	var imageLoader;
	var swiper;
	
	if(context) {
		debounce = context.debounce;
		tests = context.tests;
		imageLoader = context.imageLoader;
		swiper = context.swiper;
	} else {
		debounce = require('./scripts/debounce.js');
		tests = require('./scripts/tests.js');
		imageLoader = require('./scripts/image.loader.js');
		swiper = require('./scripts/swiper.js');
	}
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));