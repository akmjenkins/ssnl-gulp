<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section class="excerpt-block">
		<div class="sw">
			
			<div>

				<div class="hgroup centered">
					<h1 class="hgroup-title">SSNL Membership Registration</h1>
				</div><!-- .hgroup.centered -->

				<p class="excerpt">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
					Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar 
					tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
					Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien 
					nunc eget odio.
				</p>				
			</div>


		</div><!-- .sw -->
	</section>

	<section class="light-bg">
		<div class="sw">


			<form action="/" novalidate class="body-form full">

				<div class="registration-form">

					<div class="fieldset">

						<div class="hgroup section-header">
							<h3 class="hgroup-title">Payment</h3>
						</div><!-- .hgroup -->

						<div class="progress-bar">
							<span>School Information</span>
							<span>Representative Information</span>
							<span>Coach &amp; Sports</span>
							<span class="selected">Payment</span>
						</div><!-- .progress-bar -->

						<div class="fieldset-box">
							<div class="fieldset-box-head">
								<label class="checkbox">
									<input type="checkbox" name="high_school">
									<span>High School Tournament Program</span>
								</label>
							</div><!-- .fieldset-box-head -->
							<div class="fieldset-box-content">
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
									Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar 
									tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
									Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus 
									sapien nunc eget odio.
								</p>
							</div><!-- .fieldset-box-content -->
						</div><!-- .fieldset-box -->

						<div class="fieldset-box">
							<div class="fieldset-box-head">
								<label class="checkbox">
									<input type="checkbox" name="participation_nation">
									<span>Participation Nation</span>
								</label>
							</div><!-- .fieldset-box-head -->
							<div class="fieldset-box-content">
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
									Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar 
									tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
									Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus 
									sapien nunc eget odio.
								</p>
							</div><!-- .fieldset-box-content -->
						</div><!-- .fieldset-box -->

						<div class="fieldset-box">
							<div class="fieldset-box-head">
								Costs
							</div><!-- .fieldset-box-head -->
							<div class="fieldset-box-content">

								<label class="radio fieldset-box-row">
									<div class="row">
										<input type="radio" name="num_students" value="50">
										<span class="l">Less than 50 students</span>
										<span class="r">$0.00</span>
									</div><!-- .row -->
								</label>

								<label class="radio fieldset-box-row">
									<div class="row">
										<input type="radio" name="num_students" value="100">
										<span class="l">51-100 students</span>
										<span class="r">$0.00</span>
									</div><!-- .row -->
								</label>

								<label class="radio fieldset-box-row">
									<div class="row">
										<input type="radio" name="num_students" value="350">
										<span class="l">101-350 students</span>
										<span class="r">$0.00</span>
									</div><!-- .row -->
								</label>

								<label class="radio fieldset-box-row">
									<div class="row">
										<input type="radio" name="num_students" value="600">
										<span class="l">351-600 students</span>
										<span class="r">$0.00</span>
									</div><!-- .row -->
								</label>

								<label class="radio fieldset-box-row">
									<div class="row">
										<input type="radio" name="num_students" value="601">
										<span class="l">601 or more students</span>
										<span class="r">$0.00</span>
									</div><!-- .row -->
								</label>

							</div><!-- .fieldset-box-content -->

							<div class="fieldset-box-footer">
								<span class="f-right uc">
									Total Cost &mdash; <span class="total-cost">$0.00</span>
								</span>
							</div><!-- .fieldset-box-footer -->

						</div><!-- .fieldset-box -->

						<div class="fieldset-box">
							<div class="fieldset-box-head">
								Payment
							</div><!-- .fieldset-box-head -->
							<div class="fieldset-box-content">

								<div class="center">

									<label class="radio">
										<input type="radio" name="payment_type" value="visa">
										<span>
											<span class="cc-ico t-fa-abs fa-cc-visa">Visa</span>
										</span>
									</label>

									<label class="radio">
										<input type="radio" name="payment_type" value="visa">
										<span>
											<span class="cc-ico t-fa-abs fa-cc-mastercard">MasterCard</span>
										</span>
									</label>

									<label class="radio">
										<input type="radio" name="payment_type" value="visa">
										<span>
											<span class="cc-ico t-fa-abs fa-cc-paypal">PayPal</span>
										</span>
									</label>

									<label class="radio">
										<input type="radio" name="payment_type" value="visa">
										<span>Cheque</span>
									</label>

								</div><!-- .center -->

							</div><!-- .fieldset-box-content -->
						</div><!-- .fieldset-box -->

						<div class="fieldset-box">
							<div class="fieldset-box-head">
								Final Details
							</div><!-- .fieldset-box-head -->
							<div class="fieldset-box-content">

								<label class="checkbox fieldset-box-row">
									<input type="checkbox">
									<span>
										By registering, our school agrees to abide by the rules, regulation, and spirit of all 
										School Sports Newfoundland &amp; Labrador programs.
									</span>
								</label>

								<label class="checkbox fieldset-box-row">
									<input type="checkbox">
									<span>
										By registering, our school agrees to abide by the rules, regulation, and spirit of all 
										School Sports Newfoundland &amp; Labrador programs.
									</span>
								</label>

								<label class="checkbox fieldset-box-row">
									<input type="checkbox">
									<span>
										Our school aknowledges that all photos submitted by us or taken of our students by 
										representative of SSNL can be used by SSNL for promotional purposes.
									</span>
								</label>

								<label class="checkbox fieldset-box-row">
									<input type="checkbox">
									<span>
										I acknowledge that my school's Principal has approved registering for the above 
										programs and accepts the aformentioned disclaimers.
									</span>
								</label>

							</div><!-- .fieldset-box-content -->
						</div><!-- .fieldset-box -->

						<div class="form-controls">
							<button type="button" class="button fill previous">Previous</button>
							<button type="submit" class="button fill primary next">Submit</button>
						</div><!-- .form-controls -->

					</div><!-- .fieldset -->
				
				</div><!-- .registration-form -->
	
			</form>
			

		</div><!-- .sw -->
	</section><!-- .light-bg -->

	<section class="d-bg primary-bg">
		<div class="sw">
			
			<?php include('inc/i-latest-tweet.php'); ?>

		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>