<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section class="excerpt-block">
		<div class="sw">
			
			<div>

				<div class="hgroup centered">
					<h1 class="hgroup-title">SSNL Membership Registration</h1>
				</div><!-- .hgroup.centered -->

				<p class="excerpt">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
					Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar 
					tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
					Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien 
					nunc eget odio.
				</p>				
			</div>


		</div><!-- .sw -->
	</section>

	<section class="light-bg">
		<div class="sw">


			<form action="/" novalidate class="body-form full">

				<div class="registration-form">

					<div class="fieldset">

						<div class="hgroup section-header">
							<h3 class="hgroup-title">Representative Information</h3>
						</div><!-- .hgroup -->

						<div class="progress-bar">
							<span>School Information</span>
							<span>Representative Information</span>
							<span class="selected">Coach &amp; Sports</span>
							<span>Payment</span>
						</div><!-- .progress-bar -->						


						<div class="hgroup">
							<h2 class="hgroup-title">School Name Goes Here</h2>
						</div><!-- .hgroup -->

						<p class="center">
							Coach and Teacher-Sponsor information is automatically carried over from the previous school year. 
							Please select the sports that require changes and update your coaches and teacher-sponsors or use 
							the master list drop down to  choose a coach from a previous year.
						</p>

						<br>
						<br>

						<fieldset>
							<legend>Select Sports</legend>

							<div class="fieldset-box">
								<div class="fieldset-box-head">
									<span>Select Sport</span>
								</div><!-- .fieldset-box-head -->
								<div class="fieldset-box-content cf">
									
									<div class="grid pad10 collapse-599">
										<div class="col col-1">
											<div class="item">
												<div class="selector light with-arrow">
													<select name="teacher_sponsor[]">
														<option value="">Teacher Sponsor</option>
														<option value="1">Teacher Option One</option>
														<option value="2">Teacher Option Two</option>
														<option value="3">Teacher Option Three</option>
													</select>
													<span class="value"></span>
												</div><!-- .selector -->
											</div><!-- .item -->
										</div><!-- .col -->
										<div class="col col-1">
											<div class="item">
												<input type="email" name="teacher_sponsor_email[]" placeholder="Teacher Sponsor's E-mail">
											</div><!-- .item -->
										</div><!-- .col -->
										<div class="col col-1">
											<span class="form-action form-action-add">Add Teacher Sponsor</span>

											<small class="block">* A Teacher Sponsor is mandatory for all SSNL Events if someone on the coaching staff is not a member.</small>
										</div>
									</div><!-- .grid -->

									<br>

									<div class="grid pad10 collapse-599">
										<div class="col col-1">
											<div class="item">
												<div class="selector light with-arrow">
													<select name="coach[]">
														<option value="">Coach</option>
														<option value="1">Coach Option One</option>
														<option value="2">Coach Option Two</option>
														<option value="3">Coach Option Three</option>
													</select>
													<span class="value"></span>
												</div><!-- .selector -->
											</div><!-- .item -->
										</div><!-- .col -->
										<div class="col col-1">
											<div class="item">
												<input type="email" name="coach_email[]" placeholder="Coach's E-mail">
											</div><!-- .item -->
										</div><!-- .col -->
										<div class="col col-1">
											<span class="form-action form-action-add">Add Coach</span>
										</div>
									</div><!-- .grid -->

									<button type="button" class="f-right button fill primary">Done</button>

								</div><!-- .fieldset-box-content -->
							</div><!-- .fieldset-box -->
						</fieldset>

						<div class="form-controls">
							<button type="button" class="button fill previous">Previous</button>
							<button type="submit" class="button fill primary next">Next</button>
						</div><!-- .form-controls -->

					</div><!-- .fieldset -->
				
				</div><!-- .registration-form -->
	
			</form>
			

		</div><!-- .sw -->
	</section><!-- .light-bg -->

	<section class="d-bg primary-bg">
		<div class="sw">
			
			<?php include('inc/i-latest-tweet.php'); ?>

		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>