<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section class="excerpt-block">
		<div class="sw">
			
			<div>

				<div class="hgroup centered">
					<h1 class="hgroup-title">Sports</h1>
				</div><!-- .hgroup.centered -->

				<p class="excerpt">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
					Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar 
					tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
					Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien 
					nunc eget odio.
				</p>				
			</div>


		</div><!-- .sw -->
	</section>

	<section class="light-bg">
		<div class="sw">

				<div class="hgroup centered section-header">
					<h2 class="hgroup-title">Yearly Calendar</h2>
				</div><!-- .hgroup.centered -->

			<div class="calendar-buttons">
				
				<a href="#" class="button primary fill">Back to Sports Page</a>

				<div class="calendar-selectors">

					<div class="selector with-arrow">
						<select>
							<option value="">Select Month</option>
							<option value="1">January</option>
							<option value="2">February</option>
							<option value="3">March</option>
							<option value="4">April</option>
							<option value="5">May</option>
							<option value="6">June</option>
							<option value="7">July</option>
							<option value="8">August</option>
							<option value="9">September</option>
							<option value="10">October</option>
							<option value="11">November</option>
							<option value="12">December</option>
						</select>
						<span class="value"></span>
					</div><!-- .selector -->

					<div class="selector with-arrow">
						<select>
							<option value="">Select Sport</option>
							<option value="1">January</option>
							<option value="2">February</option>
							<option value="3">March</option>
							<option value="4">April</option>
							<option value="5">May</option>
							<option value="6">June</option>
							<option value="7">July</option>
							<option value="8">August</option>
							<option value="9">September</option>
							<option value="10">October</option>
							<option value="11">November</option>
							<option value="12">December</option>
						</select>
						<span class="value"></span>
					</div><!-- .selector -->

				</div><!-- .calendar-selectors -->

			</div><!-- .calendar-buttons -->

			<div class="calendar-month-view">
				
				<div class="calendar-month-head">
					<span class="calendar-month-name">January</span>
					<a href="#" class="inline">View Calendar</a>
				</div><!-- .calendar-month-head -->

				<div class="calendar-month-row">
					<div class="calendar-month-time">
						<time>
							<span class="m">Jan</span>
							<span class="d">03</span>
							<span class="y">2015</span>
						</time>
					</div><!-- .calendar-month-time -->
					<div class="calendar-month-title">
						<span class="block">Junior High Division</span>
						<span class="block">A Basketball Tournament</span>
					</div><!-- .calendar-month-title -->
					<div class="calendar-month-loc">
						<span class="block">Holy Spirit High School</span>
						<span class="block">Conception Bay South</span>
					</div><!-- .calendar-month-loc -->
					<div class="calendar-month-button">
						<a href="#" class="button fill primary">View Details</a>
					</div><!-- .calendar-month-button -->
				</div><!-- .calendar-month-row -->

				<div class="calendar-month-row">
					<div class="calendar-month-time">
						<time>
							<span class="m">Jan</span>
							<span class="d">03</span>
							<span class="y">2015</span>
						</time>
					</div><!-- .calendar-month-time -->
					<div class="calendar-month-title">
						<span class="block">Junior High Division</span>
						<span class="block">A Basketball Tournament</span>
					</div><!-- .calendar-month-title -->
					<div class="calendar-month-loc">
						<span class="block">Holy Spirit High School</span>
						<span class="block">Conception Bay South</span>
					</div><!-- .calendar-month-loc -->
					<div class="calendar-month-button">
						<a href="#" class="button fill primary">View Details</a>
					</div><!-- .calendar-month-button -->
				</div><!-- .calendar-month-row -->

				<div class="calendar-month-row">
					<div class="calendar-month-time">
						<time>
							<span class="m">Jan</span>
							<span class="d">03</span>
							<span class="y">2015</span>
						</time>
					</div><!-- .calendar-month-time -->
					<div class="calendar-month-title">
						<span class="block">Junior High Division</span>
						<span class="block">A Basketball Tournament</span>
					</div><!-- .calendar-month-title -->
					<div class="calendar-month-loc">
						<span class="block">Holy Spirit High School</span>
						<span class="block">Conception Bay South</span>
					</div><!-- .calendar-month-loc -->
					<div class="calendar-month-button">
						<a href="#" class="button fill primary">View Details</a>
					</div><!-- .calendar-month-button -->
				</div><!-- .calendar-month-row -->

				<div class="calendar-month-row">
					<div class="calendar-month-time">
						<time>
							<span class="m">Jan</span>
							<span class="d">03</span>
							<span class="y">2015</span>
						</time>
					</div><!-- .calendar-month-time -->
					<div class="calendar-month-title">
						<span class="block">Junior High Division</span>
						<span class="block">A Basketball Tournament</span>
					</div><!-- .calendar-month-title -->
					<div class="calendar-month-loc">
						<span class="block">Holy Spirit High School</span>
						<span class="block">Conception Bay South</span>
					</div><!-- .calendar-month-loc -->
					<div class="calendar-month-button">
						<a href="#" class="button fill primary">View Details</a>
					</div><!-- .calendar-month-button -->
				</div><!-- .calendar-month-row -->

			</div><!-- .calendar-month-view -->

			<div class="calendar-month-view">
				
				<div class="calendar-month-head">
					<span class="calendar-month-name">February</span>
					<a href="#" class="inline">View Calendar</a>
				</div><!-- .calendar-month-head -->

				<div class="calendar-month-row">
					<div class="calendar-month-time">
						<time>
							<span class="m">Jan</span>
							<span class="d">03</span>
							<span class="y">2015</span>
						</time>
					</div><!-- .calendar-month-time -->
					<div class="calendar-month-title">
						<span class="block">Junior High Division</span>
						<span class="block">A Basketball Tournament</span>
					</div><!-- .calendar-month-title -->
					<div class="calendar-month-loc">
						<span class="block">Holy Spirit High School</span>
						<span class="block">Conception Bay South</span>
					</div><!-- .calendar-month-loc -->
					<div class="calendar-month-button">
						<a href="#" class="button fill primary">View Details</a>
					</div><!-- .calendar-month-button -->
				</div><!-- .calendar-month-row -->

				<div class="calendar-month-row">
					<div class="calendar-month-time">
						<time>
							<span class="m">Jan</span>
							<span class="d">03</span>
							<span class="y">2015</span>
						</time>
					</div><!-- .calendar-month-time -->
					<div class="calendar-month-title">
						<span class="block">Junior High Division</span>
						<span class="block">A Basketball Tournament</span>
					</div><!-- .calendar-month-title -->
					<div class="calendar-month-loc">
						<span class="block">Holy Spirit High School</span>
						<span class="block">Conception Bay South</span>
					</div><!-- .calendar-month-loc -->
					<div class="calendar-month-button">
						<a href="#" class="button fill primary">View Details</a>
					</div><!-- .calendar-month-button -->
				</div><!-- .calendar-month-row -->

				<div class="calendar-month-row">
					<div class="calendar-month-time">
						<time>
							<span class="m">Jan</span>
							<span class="d">03</span>
							<span class="y">2015</span>
						</time>
					</div><!-- .calendar-month-time -->
					<div class="calendar-month-title">
						<span class="block">Junior High Division</span>
						<span class="block">A Basketball Tournament</span>
					</div><!-- .calendar-month-title -->
					<div class="calendar-month-loc">
						<span class="block">Holy Spirit High School</span>
						<span class="block">Conception Bay South</span>
					</div><!-- .calendar-month-loc -->
					<div class="calendar-month-button">
						<a href="#" class="button fill primary">View Details</a>
					</div><!-- .calendar-month-button -->
				</div><!-- .calendar-month-row -->

				<div class="calendar-month-row">
					<div class="calendar-month-time">
						<time>
							<span class="m">Jan</span>
							<span class="d">03</span>
							<span class="y">2015</span>
						</time>
					</div><!-- .calendar-month-time -->
					<div class="calendar-month-title">
						<span class="block">Junior High Division</span>
						<span class="block">A Basketball Tournament</span>
					</div><!-- .calendar-month-title -->
					<div class="calendar-month-loc">
						<span class="block">Holy Spirit High School</span>
						<span class="block">Conception Bay South</span>
					</div><!-- .calendar-month-loc -->
					<div class="calendar-month-button">
						<a href="#" class="button fill primary">View Details</a>
					</div><!-- .calendar-month-button -->
				</div><!-- .calendar-month-row -->

			</div><!-- .calendar-month-view -->


		</div><!-- .sw -->
	</section>

	<section class="d-bg primary-bg">
		<div class="sw">
			
			<?php include('inc/i-latest-tweet.php'); ?>

		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>