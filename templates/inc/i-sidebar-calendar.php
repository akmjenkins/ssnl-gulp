<div class="sidebar-mod calendar-mod">

	<div class="calendar-mod-head">
		<span class="calendar-mod-title">Calendar of Events</span>
		<a href="#">View Yearly Calendar</a>
	</div><!-- .calendar-mod-head -->

	<!-- 
		data-src is the URL to get a list of events from when passed GET parameters
		of ?month={{num}}&year={{num}} - this is used to add red underlines
		to dates that have events
	-->

	<div class="events-calendar d-bg" data-src="./inc/i-sample-events.php">
		<a href="#link" class="monthly inline">View Monthly Calendar</a>
	</div><!-- .events-calendar -->

</div><!-- .sidebar-mod -->