<?php

    ini_set('display_errors','On');

	/**
	
	
		NOTE: 
			You do not need to (and probably won't) be using this file, or a file like it in production.
			
			Your event source will probably be contained "within wordpress" somewhere, rather
			than within the template.

			It really doesn't make a difference, as long as the same
			json is returned when either an id parameter, or a month/year parameter is passed
			as arguments

            The content of the event objects can vary, but you must include the following:
                id - integer
                month - integer
                year - integer
                day - integer
	
	
	*/


//sample events
$events = array(
        array(
            'id' => 1,
            'excerpt' => 'Vivamus posuere elementum leo ac feugiat. Morbi consectetur, quam sed tincidunt ultricies, libero felis semper nisi, quis facilisis erat ligula non enim. Pellentesque luctus mattis orci vitae tincidunt. Maecenas sollicitudin gravida nunc viverra feugiat.',
            'permalink' => 'http://someurl',
            'title' => 'Event Title',
			'month' => 5,
			'year' => 2015,
            'day' => 4
        ),
        array(
            'id' => 2,
            'excerpt' => 'Vivamus posuere elementum leo ac feugiat. Morbi consectetur, quam sed tincidunt ultricies, libero felis semper nisi, quis facilisis erat ligula non enim. Pellentesque luctus mattis orci vitae tincidunt. Maecenas sollicitudin gravida nunc viverra feugiat.',
            'permalink' => 'http://someurl',
            'title' => 'Event Title',
            'month' => 5,
            'year' => 2015,
            'day' => 4
        ),
        array(
            'id' => 3,
            'excerpt' => 'Vivamus posuere elementum leo ac feugiat. Morbi consectetur, quam sed tincidunt ultricies, libero felis semper nisi, quis facilisis erat ligula non enim. Pellentesque luctus mattis orci vitae tincidunt. Maecenas sollicitudin gravida nunc viverra feugiat.',
            'permalink' => 'http://someurl',
            'title' => 'Event Title',
            'month' => 5,
            'year' => 2015,
            'day' => 5
        ),
        array(
             'id' => 4,
            'excerpt' => 'Vivamus posuere elementum leo ac feugiat. Morbi consectetur, quam sed tincidunt ultricies, libero felis semper nisi, quis facilisis erat ligula non enim. Pellentesque luctus mattis orci vitae tincidunt. Maecenas sollicitudin gravida nunc viverra feugiat.',
            'permalink' => 'http://someurl',
            'title' => 'Event Title',
            'month' => 5,
            'year' => 2015,
            'day' => 6
        ),
        array(
            'id' => 5,
            'excerpt' => 'Vivamus posuere elementum leo ac feugiat. Morbi consectetur, quam sed tincidunt ultricies, libero felis semper nisi, quis facilisis erat ligula non enim. Pellentesque luctus mattis orci vitae tincidunt. Maecenas sollicitudin gravida nunc viverra feugiat.',
            'permalink' => 'http://someurl',
            'title' => 'Event Title',
            'month' => 5,
            'year' => 2015,
            'day' => 20
        ),
        array(
            'id' => 6,
            'excerpt' => 'Vivamus posuere elementum leo ac feugiat. Morbi consectetur, quam sed tincidunt ultricies, libero felis semper nisi, quis facilisis erat ligula non enim. Pellentesque luctus mattis orci vitae tincidunt. Maecenas sollicitudin gravida nunc viverra feugiat.',
            'permalink' => 'http://someurl',
            'title' => 'Event Title',
            'month' => 5,
            'year' => 2015,
            'day' => 29
        ),
        array(
            'id' => 7,
            'excerpt' => 'Vivamus posuere elementum leo ac feugiat. Morbi consectetur, quam sed tincidunt ultricies, libero felis semper nisi, quis facilisis erat ligula non enim. Pellentesque luctus mattis orci vitae tincidunt. Maecenas sollicitudin gravida nunc viverra feugiat.',
            'permalink' => 'http://someurl',
            'title' => 'Event Title',
            'month' => 5,
            'year' => 2015,
            'day' => 30
        ),
        array(
            'id' => 8,
            'excerpt' => 'Vivamus posuere elementum leo ac feugiat. Morbi consectetur, quam sed tincidunt ultricies, libero felis semper nisi, quis facilisis erat ligula non enim. Pellentesque luctus mattis orci vitae tincidunt. Maecenas sollicitudin gravida nunc viverra feugiat.',
            'permalink' => 'http://someurl',
            'title' => 'Event Title',
            'month' => 5,
            'year' => 2015,
            'day' => 31
        ),
        array(
            'id' => 9,
            'excerpt' => 'Vivamus posuere elementum leo ac feugiat. Morbi consectetur, quam sed tincidunt ultricies, libero felis semper nisi, quis facilisis erat ligula non enim. Pellentesque luctus mattis orci vitae tincidunt. Maecenas sollicitudin gravida nunc viverra feugiat.',
            'permalink' => 'http://someurl',
            'title' => 'Event Title',
            'month' => 6,
            'year' => 2015,
            'day' => 1
        ),
        array(
            'id' => 10,
            'excerpt' => 'Vivamus posuere elementum leo ac feugiat. Morbi consectetur, quam sed tincidunt ultricies, libero felis semper nisi, quis facilisis erat ligula non enim. Pellentesque luctus mattis orci vitae tincidunt. Maecenas sollicitudin gravida nunc viverra feugiat.',
            'permalink' => 'http://someurl',
            'title' => 'Event Title',
            'month' => 6,
            'year' => 2015,
            'day' => 2
        ),
        array(
            'id' => 11,
            'excerpt' => 'Vivamus posuere elementum leo ac feugiat. Morbi consectetur, quam sed tincidunt ultricies, libero felis semper nisi, quis facilisis erat ligula non enim. Pellentesque luctus mattis orci vitae tincidunt. Maecenas sollicitudin gravida nunc viverra feugiat.',
            'permalink' => 'http://someurl',
            'title' => 'Event Title',
            'month' => 6,
            'year' => 2015,
            'day' => 3
        ),
        array(
            'id' => 12,
            'excerpt' => 'Vivamus posuere elementum leo ac feugiat. Morbi consectetur, quam sed tincidunt ultricies, libero felis semper nisi, quis facilisis erat ligula non enim. Pellentesque luctus mattis orci vitae tincidunt. Maecenas sollicitudin gravida nunc viverra feugiat.',
            'permalink' => 'http://someurl',
            'title' => 'Event Title',
            'month' => 6,
            'year' => 2015,
            'day' => 10
        ),
        array(
            'id' => 13,
            'excerpt' => 'Vivamus posuere elementum leo ac feugiat. Morbi consectetur, quam sed tincidunt ultricies, libero felis semper nisi, quis facilisis erat ligula non enim. Pellentesque luctus mattis orci vitae tincidunt. Maecenas sollicitudin gravida nunc viverra feugiat.',
            'permalink' => 'http://someurl',
            'title' => 'Event Title',
            'month' => 6,
            'year' => 2015,
            'day' => 24
        ),
        array(
            'id' => 14,
            'excerpt' => 'Vivamus posuere elementum leo ac feugiat. Morbi consectetur, quam sed tincidunt ultricies, libero felis semper nisi, quis facilisis erat ligula non enim. Pellentesque luctus mattis orci vitae tincidunt. Maecenas sollicitudin gravida nunc viverra feugiat.',
            'permalink' => 'http://someurl',
            'title' => 'Event Title',
            'month' => 7,
            'year' => 2015,
            'day' => 4
        ),
        array(
            'id' => 15,
            'excerpt' => 'Vivamus posuere elementum leo ac feugiat. Morbi consectetur, quam sed tincidunt ultricies, libero felis semper nisi, quis facilisis erat ligula non enim. Pellentesque luctus mattis orci vitae tincidunt. Maecenas sollicitudin gravida nunc viverra feugiat.',
            'permalink' => 'http://someurl',
            'title' => 'Event Title',
            'month' => 7,
            'year' => 2015,
            'day' => 8
        )
);
	
	class SSNLEvents {
		
		public static function getEventById($id) {
			global $events;
			foreach($events as $event) {
				if($event['id'] === $id) {
					return $event;
				}
			}
			
			return array();
		}
		
		public static function getEventIdsForMonthYear($month,$year) {
			global $events;
			$eventData = array();
			
			foreach($events as $event) {
				if($event['month'] == $month && $event['year'] == $year) {
					$eventData[] = $event;
				}
				
			}
			
			return $eventData;
		}
		
	}
	
	header('Content-type: application/json');
	if(isset($_GET['id'])) {
		echo json_encode(SSNLEvents::getEventById($_GET['id']));
	} else if(isset($_GET['month']) && isset($_GET['year'])) {
		echo json_encode(SSNLEvents::getEventIdsForMonthYear($_GET['month'],$_GET['year']));
	} else {
		echo json_encode(array());
	}
	
	exit;